import axios from 'axios';

const url = 'https://restcountries.eu/rest/v2/all';

class DataService {

    async getAllCountry() {
        const res = await axios.get(url)
        console.log(res);
    }


}

export default DataService;